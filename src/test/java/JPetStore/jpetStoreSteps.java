package JPetStore;

import io.cucumber.java.en.*;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import  org.openqa.selenium.chrome.ChromeDriver;

import io.cucumber.java.en.Given;

import static org.junit.Assert.assertTrue;

public class jpetStoreSteps {

    WebDriver driver;

    @Given("un navigateur est ouvert")
    public void un_navigateur_est_ouvert() {
        System.setProperty("webdriver.chrome.driver", "./rsc/chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
        driver.get("https://www.google.fr");
        driver.manage().window().maximize();
    }
    @Given("je suis sur url {string}")
    public void je_suis_sur_l_url(String url) {
        driver.get(url);
    }
    @When("je clique sur le lien de connexion")
    public void je_clique_sur_le_lien_de_connexion() {
        driver.findElement(By.xpath("//a[contains(text(),\"Sign In\")]")).click();
    }
    @When("rentre le username {string}")
    public void rentre_le_username(String name) throws InterruptedException {
        driver.findElement(By.name("username")).clear();
        driver.findElement(By.name("username")).sendKeys(name);
    }
    @When("rentre le password {string}")
    public void rentre_le_password(String pw) {
        driver.findElement(By.name("password")).clear();
        driver.findElement(By.name("password")).sendKeys(pw);
    }
    @When("je clique sur login")
    public void je_clique_sur_login() {
        driver.findElement(By.name("signon")).click();
    }
    @Then("utilisateur ABC est connecte")
    public void utilisateur_ABC_est_connecte() {
        boolean connected = driver.findElement(By.xpath("//a[contains(text(),\"Sign Out\")]")).isDisplayed();
        assertTrue(connected);
    }
    @Then("je peux lire le message d'accueil {string}")
    public void je_peux_lire_le_message_d_accueil(String welcomeMessage) {
        String message = driver.findElement(By.id("WelcomeContent")).getText();
        assertTrue(message.contains(welcomeMessage));
    }
    @Then("je ferme le navigateur")
    public void je_ferme_le_navigateur() {
        driver.close();
        driver.quit();
    }

}
