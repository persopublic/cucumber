Feature: Connexion � l'application Jpetstore

  Scenario Outline: Connexion
    Given un navigateur est ouvert
    And je suis sur url "https://petstore.octoperf.com/actions/Catalog.action"
    When je clique sur le lien de connexion
    And rentre le username <login>
    And rentre le password <password>
    And je clique sur login
    Then utilisateur ABC est connecte
    And  je peux lire le message d'accueil "Welcome ABC!"
    And je ferme le navigateur



    Examples:
      |login | password |
      | "j2ee" | "j2ee"     |
      | "ACID" | "ACID"     |
